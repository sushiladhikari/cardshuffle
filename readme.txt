=Installation=

Download the plugin from above link and install on your WordPress directory
Create new page/post and paste this shortcode on the page content
[cardshuffle]
This will shuffle one deck(52cards) among five players

3. You can shuffle any number of deck to any number of players by just adding these argument on above shortcode

[cardshuffle deck=”3″ players=”6″]

4. Finally get the hands of card on the desired players. Here the above output shows all the card that first players got in his hand. You can do it simply passing adding paramter on card_on_player_hand arguments.

[cardshuffle deck=”3″ players=”6″ card_on_player_hand=”1″]