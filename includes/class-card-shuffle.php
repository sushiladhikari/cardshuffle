<?php
/**
* News corp setup
*/
if( !defined( 'ABSPATH' ) ) {
	exit();
}

class CardShuffle {
	
	/**
	 * Newcorporation Constructor.
	 */
	public function __construct() {
		$this->define_constants();
		$this->includes();
	}


	/**
	 * Define WC Constants.
	 */
	private function define_constants() {
		//defines abspath of the plugins
		define( 'CS_ABSPATH', dirname( CS_PLUGIN_FILE ) . '/' );
	}

	/**
	 * Include required core files 
	 */
	public function includes() {
		//add card generation
		require_once CS_ABSPATH . 'includes/class-generate-random-hand.php';
		//functional file
		require_once CS_ABSPATH . 'functions.php';
	}

}