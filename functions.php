<?php
/**
* This file includes all the functional aspects of this function
* @author Sushil Adhikari
* @version 1.0
*/


/**
* @return array of shuffle card
* @author Sushil Adhikari
* @param array consisting deck number ,total number of players in the game and player queue number to extract card on hand
*/
function display_shuffle_card( $atts ){
	$card_args = shortcode_atts( array(
		'deck' => 1,
		'players' => 1,
		'card_on_player_hand' => 1
	), $atts );

	$deck = absint( $card_args[ 'deck' ] );
	$players = absint( $card_args[ 'players' ] );
	$player_hand = absint( $card_args[ 'card_on_player_hand' ] );

	$GenerateRandomHand = new GenerateRandomHand( $deck, $players );

	//create card deck 
	$is_valid_entry = $GenerateRandomHand->validate_entry();
	if( false === $is_valid_entry ) {
		echo $GenerateRandomHand->get_error_message();
		return false;
	}
	$create_card_deck = $GenerateRandomHand->create_card_deck();
	$error = $GenerateRandomHand->get_error_message();
	$generate_card = $GenerateRandomHand->get_deck_of_card();

	if( !empty( $error ) ) {
		echo $GenerateRandomHand->get_error_message();
		return false;
	}
	$card_on_team_hand = $GenerateRandomHand->destribute_deck_on_team_players();
	//format card of first player
	$format_player_card = $GenerateRandomHand->format_players_card( $card_on_team_hand, 1 );
	if( false === $format_players_card ) {
		echo $GenerateRandomHand->get_error_message();
		return false;
	} else {
		printf( esc_html__( 'Card on player : %1$d are %2$s', 'cardshuffle' ), $player_hand, implode( ', ', $format_player_card ) );
	}
}
add_shortcode( 'cardshuffle', 'display_shuffle_card' );